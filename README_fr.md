# vestibule

Vestibule est un système de connexion à un service web.

Il permet de 
- créer un compte (avec vérification automatique par l'envoi d'un mail)
- modifier son mot de passe

Il fournit un moyen très simple de savoir si un utilisateur est connecté par une variable de session, permettant de créer facilement des services.

Le répertoire où sont stockés les comptes est paramétrable, ce qui permet de réaliser facilement un système de SSO pour les applications web.

# Requisits

Vestibule est codé en php8.

Il n'est pas nécessaire que le serveur puisse envoyer des mail. S'il ne peut pas envoyer de mail, le système déposera dans un compte la demande de compte et le reste de la création du compte se fera via la personne qui consultera ce compte.

## Mécanisme

Au moment de la demande de création du compte un token est créé dans le répertoire temporaire du système. 

Les comptes sont créés dans un répertoire *accounts*.  Ils contiennent les données minimales du compte utilisateur. Habituellement les services rangent les données des utilisateurs dans un répertoires *users*. Vestibule n'a pas besoin de connaître le répertoire des données des utilisateurs.

## Sécurité

L'accès au répertoire *account* est protégé par un *index.html* vide et par un .htaccess. Il faut vérifier que le serveur (nginx ou apache par exemple) lit bien les .htaccess (voir la directive AllowOverride) 

# Logique

![logic](logic.svg)

# Installation

Placez le répertoire *vestibule* de façon à ce que le serveur web puisse y accéder.

# Utilisation

Placer dans le *index.php* de votre service web les lignes suivante:

> $vestibule_accounts='/web/dev/testV/accounts'; // accounts directory - maybe outside for sso
> $vestibule_users='/web/dev/testV/users';       // accounts directory (only for admin purpose if server doesn't send mail)
> $vestibule_path='../vestibule';                // relative link to the vestibule directory 
> $vestibule_service_name='Nom du service';      // name of your service
> require("$vestibule_path/vestibule.php");
> 
> // to understant
> 
> if ($user=='')                                 // in fact $_SESSION['username'];
>    echo _('Content for non connected users');
> else
>    echo _('Content for connected users');

> echo $vestibule_form;                          // form to subscribe/connect/disconnect

# Licence 

GPL v3 Affero

## Usage fpour les développeurs

Si l'utilisateur est connecté, $_SESSION['Username'] contient the username.

## Goodies

System of translation. Be inspired, it's so simple. Poedit is your friend.
