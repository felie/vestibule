<?php

$vestibule_server='http'.((isset($_SERVER['HTTPS']) and $_SERVER['HTTPS']=='on')?'s':'').'://'.$_SERVER['HTTP_HOST'];
if ($vestibule_debug_mode) echo "vestibule_server: $vestibule_server<br/>";
$vestibule_website=dirname($_SERVER['SCRIPT_NAME']);

$vestibule_website_url="$vestibule_server$vestibule_website";
$vestibule_homepage=$vestibule_website_url;
$vestibule_url="$vestibule_server$vestibule_website";
if ($vestibule_debug_mode) echo "vestibule_url: $vestibule_url<br/>";
$vestibule_duration=3000;// time()+60*60*6; // 6 heures

if (!isset($rootsite4vestibule))
    $rootsite4vestibule=getcwd();
//echo $rootsite4vestibule;
if (!isset($accounts4vestibule))
    $accounts4vestibule='accounts';

$vestibule_path="$rootsite4vestibule/../vestibule";
$vestibule_accounts="$rootsite4vestibule/$accounts4vestibule";

$vestibule_login_page="index.php";
$vestibule_logout_page="index.php"; // ok

$vestibule_name=_('The admin');

$vestibule_Mail_on=true;
$vestibule_from_mail="vestibule";
$vestibule_sign='your@domain.com';
$vestibule_helpmail='help@domain.com';

$vestibule_empty_account=['username'=>'','email'=>'','role'=>'user','password'=>''];
$vestibule_admin_account=['username'=>'admin','email'=>"$vestibule_sign",'role'=>'admin','password'=>'admin'];
?>
