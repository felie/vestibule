<?php
session_start();
?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Pikora</title>
</head>
<body>
<?php
  if (isset($_SESSION['username']))
    $content="<hr><h1>Contenu de la page privée</h1>";
  else
    $content="<hr><h1>Contenu de la page publique</h1>";
  echo $content;
?>

<?php
require('../vestibule/vestibule.php'); // gestion des utilisateurs
?>
</body>
</html>
