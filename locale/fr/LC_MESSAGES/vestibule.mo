��            )   �      �     �     �  �   �     �     �     �  =   �  #   �  �     ,   �  
   +     6     9     U     ^     p     �     �     �  	   �     �     �     �     �          '     :     A  J  I     �     �  �   �     �     �     �  L   �  0   !	    R	  M   X
  
   �
     �
     �
     �
     �
  (   �
  !   '     I     `     h     y     �  #   �  *   �     �     �                             	                                                                                        
                                Account Account created An email has just left for you to complete the registration procedure.<br/>It contains a link that will outdated in 2 hours.<br/>If the message does not reach you (look in the spam box), do not hesitate to write to %s Confirmation of the password Desired Username Email Existed username. If it' yours, you may change your password. File not found - link maybe too old Hi,

You (or someone for you) have requested a registration on %s, or the reset of your password

Your login is: <b>%s</b>

Below you will find a single use link with a 2 hour lapse to initialize your account
%s

Best regards
%s Message left to admin, he'll get back to you My Account OK Opening of an account on %s Password Password set for  Passwords are not identical Please define a password Requested disconnection Send The admin Unauthorized email User Username available Username or Password invalid Vestibule message: Your are connected logout timeout Project-Id-Version: logintool 0.1
PO-Revision-Date: 2024-12-26 20:35+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 Compte Compte créé Un courriel vient de partir pour vous, afin de compléter la procédure d'inscription.<br/>Il contient un lien valable 2 heures.<br>Si le message ne vous parvient pas (regarder dans la boite de spam) ne pas hésiter à écrire à %s Confirmation du mot de passe Nom d'utilisateur souhaité Courriel Utilisateur existant. Si c'est vous, vous pouvez changer votre mot de passe. Fichier introuvable, lien peut-être trop ancien Bonjour,

Vous (ou quelqu'un pour vous) avez demandé une inscription sur %s, ou le changement de votre mot de passe

Votre login sera: %s

C-dessous vous trouverez un lien à usage unique valable pour 2 heures afin d'intialiser votre compte
%s

Cordialement
%s Un message a été laissé à l'administrateur, vous devriez recevoir un mail Mon compte Envoyer Ouverture d'un compte sur %s Mot de passe Mot de passe attribué à  Les mots de passe ne sont pas identiques Merci de définir un mot de passe Déconnexion demandée Envoyer L'administrateur Email non autorisé Utilisateur Ce nom d'utilisateur est disponible Nom d'utilisateur ou mot de passe invalide Message de vestibule: Vous êtes connecté déconnexion temps écoulé 