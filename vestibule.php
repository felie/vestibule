<style>
.v90{
    width:16px;
    margin-top:55px;
    color:ivory;
    transform: rotate(-90deg);
    -moz-transform: rotate(-90deg);
    -webkit-transform: rotate(-90deg);
    -khtml-transform: rotate(-90deg);
    -ms-transform: rotate(-90deg);
}
.noborder tr td{
  border:none;
  }
.vestibule{
	background:lightgray;
	width:auto;
}
</style>
<?php

session_start();
require('config.php');
require('i18n.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_WARNING);
  
// creation of the admin account
if (!file_exists("$vestibule_accounts/admin"))
    {
    $vestibule_admin_account['password']=encrypt('encrypt','admin'); 
    file_put_contents("$vestibule_accounts/admin",serialize($vestibule_admin_account));
    }
    

if (!isset($_SESSION['username']))
    $user='';
else
    $user=$_SESSION['username'];  
function vestibule_filter_mail($mail){ // filter domain name of mail for restriction
    $fai=explode('@',$mail)[1];
    $result = preg_match('/ac-(.*)$|educagri.fr$|elie.org$/',$fai); // sample
    return $result;
    }

function generate_token(){
    return strtoupper(substr(md5(mt_rand()), 0, 40));
}

$vestibule_message=_('Hi,

You (or someone for you) have requested a registration on %s, or the reset of your password

Your login is: <b>%s</b>

Below you will find a single use link with a 2 hour lapse to initialize your account
%s

Best regards
%s');

function vestibule_send_mail($mail){
    global $vestibule_Mail_on,$vestibule_from_mail,$vestibule_message,$vestibule_accounts,$vestibule_website,$vestibule_service_name,$vestibule_website_url,$vestibule_sign,$vestibule_helpmail;
    $username=$_SESSION['desired_username']; // desired username
    $token=generate_token();
    file_put_contents(sys_get_temp_dir()."/$token",serialize(['username'=>"$username",'email'=>"$mail"]));
    $link="$vestibule_website_url?token=$token&lang=".$_SESSION['lang'];
    $message_text=sprintf($vestibule_message,"$vestibule_service_name ($vestibule_website_url)",$username,$link,$vestibule_name);
    $headers='MIME-Version: 1.0' . "\r\n";
    $headers.='Content-type: text/html; charset=UTF-8' . "\r\n";
    $headers.="From: $vestibule_from_mail <www-data@pikora.fr>\r\n";
    $headers.="Reply-to:$mailsignature\r\n";
    $message.=$message_text."\n";
    $subject=sprintf(_('Opening of an account on %s'),$website);
    if ($vestibule_Mail_on){
        echo "mail?".mail($mail,$subject,$message,$headers) and mail($vestibule_sign,$subject,$mail."\n".$message,$header);
        return sprintf(_('An email has just left for you to complete the registration procedure.<br/>It contains a link that will outdated in 2 hours.<br/>If the message does not reach you (look in the spam box), do not hesitate to write to %s'),$vestibule_helpmail);
    }
    else{
        if (!file_exists($vestibule_accounts))
            mkdir($vestibule_accounts);
        if (!file_exists("$vestibule_accounts/admin"))
            mkdir("$vestibule_accounts/admin");
        $file="$vestibule_accounts/admin/admin_messages";
        $data=file_get_contents($file);
        file_put_contents($file,"$data\nMail to send: $link to $mail ($username)");
        return _('Message left to admin, he\'ll get back to you');
        }
    }
  
$vestibule_inscription="<form method='POST'>
    <input type='hidden' name='action' value='vestibule_desired_username'/>
    <input type='submit' value='"._('Registration')."'/>
</form>";

$vestibule_accountsname_form="
<form method='POST'>
  <input type='hidden' name='action' value='vestibule_accountsname'/>
  <table class='vestibule'>
   <tr class='noborder'>
    <td style='background-color:darkgray;'><div class='v90'>Vestibule</div></td>
    <td>
     <table class='noborder'>
      <tr><td align=right>"._('User').": </td><td><input type='text' name='username' value='' /></td></tr>
      <tr><td align=right>"._('Password').": </td><td><input type='password' name='password' value='' /></td></tr>
      <tr><td></td><td class='noborder' align=right><input type='submit' value='"._('OK')."' /></td></tr>
     </table>
   </td>
  </tr>
  </table>
</form>".$vestibule_inscription;

$vestibule_my_account="<form method='POST'>
    <input type='hidden' name='action' value='vestibule_my_account'/>
    <input type='submit' value='"._('My Account')."'/>
</form>";

$vestibule_email_form="
<form method='POST'>
  <table>
  <tr><td align=right>"._('Email').": </td><td><input autofocus type='text' size=40 name='mail' value='' /></td></tr>
  <input type='hidden' name='action' value='vestibule_mail'/>
  <tr><td></td><td align=right><input type='submit' name='username' value='"._('Send')."' /></td></tr>
  </table>
</form>";

$vestibule_desired_username_form="
<form method='POST'>
  <table>
  <tr><td align=right>"._('Desired Username').": </td><td><input autofocus type='text' name='desired_username' value='' /></td></tr>
  <input type='hidden' name='action' value='vestibule_proposal_username'/>
  <tr><td></td><td align=right><input type='submit' name='username' value='"._('Send')."' /></td></tr>
  </table>
</form>";

$vestibule_set_password="
<script>
function verifpasswords(){
    let value = (document.getElementById('pw1').value==document.getElementById('pw2').value);
    if (!value)
        alert('"._('Passwords are not identical')."');
    return value;
}
</script>
<form class='vestibule' method='POST'>
  %s
  <table>
  <tr><td align=right>"._('Password').": </td><td><input id='pw1' autofocus type='password' name='password1' value='' /></td></tr>
  <tr><td align=right>"._('Confirmation of the password').": </td><td><input id='pw2' type='password' name='password2' value='' /></td></tr>
  <input type='hidden' name='action' value='vestibule_password_set'/>
  <tr><td></td><td align=right><input onclick='return verifpasswords();' type='submit' name='username' value='"._('Send')."' /></td></tr>
  </table>
</form>";

$vestibule_logout="<form method='POST'>[$user]
    <input type='hidden' name='action' value='vestibule_logout'/>
    <input type='submit' value='"._('logout')."'/>
</form>";

if ($user=='')
    $vestibule_form=$vestibule_accountsname_form;
else
    $vestibule_form=$vestibule_logout;
$vestibule_form='<center>'.$vestibule_form.'<center>';
echo $vestibule_form;
    
if ($_SESSION['vestibule_message']!='')
    echo _('Vestibule message:').' '._($_SESSION['vestibule_message']).'<br/>';
$_SESSION['vestibule_message']='';
    
// managing states 
if (isset($_SESSION['next_action'])){
    $_POST['action']=$_SESSION['next_action'];
    unset($_SESSION['next_action']);
    }


if (isset($_GET['token']))
    $_POST['action']='token_control';

if (isset($_POST['action']))
switch ($_POST['action']){
    case 'vestibule_accountsname':
      $userfile="$vestibule_accounts/".$_POST['username'];
      if (file_exists($userfile) 
            and password_verify($_POST['password'],unserialize(file_get_contents($userfile))['password']))
        {
        $_SESSION['username']=$_POST['username'];
        setcookie('duration','dummy', $duration, '/'); // 
        $_SESSION['vestibule_message']=_('Your are connected');
        }
      else
        $_SESSION['vestibule_message']=_('Username or Password invalid');
      header("Location: $vestibule_website");
      exit;
    case 'vestibule_logout':
        foreach($_COOKIE as $cookie_nom=>$cookie_valeur) // delete all cookies (in fact, two: PHPSESSID and duration)
            unset($_COOKIE[$cookie_nom]);
        unset($_SESSION['username']);
        $_SESSION['vestibule_message']=_('Requested disconnection');
        header("Location: $vestibule_logout_page");
        exit;
    case 'vestibule_desired_username':echo $vestibule_desired_username_form;exit;
    case 'vestibule_proposal_username':
        if (!file_exists($vestibule_accounts)){
            mkdir($vestibule_accounts);
            file_put_contents("$vestibule_accounts/.htaccess",'Deny from all');
        }
        if (file_exists("$vestibule_accounts/".$_POST['desired_username']))//{
            $_SESSION['vestibule_message']=_('Existed username. If it\' yours, you may change your password.');
        //    $_SESSION['next_action']='vestibule_desired_username';
        //    header("Location: $vestibule_website");
        //}
        else//{
            $_SESSION['vestibule_message']=_('Username available');
            $_SESSION['desired_username']=$_POST['desired_username'];
            $_SESSION['next_action']='vestibule_inscription';
            header("Location: $vestibule_website");
        //}
        exit;
    case 'token_control':
        $tokenfile=sys_get_temp_dir()."/".$_GET['token'];
        if (file_exists($tokenfile)){
            $data=unserialize(file_get_contents($tokenfile));
            $_SESSION['desired_username']=$data['username'];
            $_SESSION['email']=$data['email'];
            $_SESSION['next_action']='vestibule_set_password';
            header("Location: $vestibule_homepage");
            }
        else
            echo _('File not found - link maybe too old');
        exit;
    case 'vestibule_set_password':
            echo '<br><b>'.$_SESSION['username'].'</b>'.sprintf($vestibule_set_password,_('Please define a password'));
        exit;
    case 'vestibule_password_set':
        $username=$_SESSION['desired_username'];
        $userfile="$vestibule_accounts/$username";
        if (!file_exists($userfile)) // account creation
            {
            $data=$vestibule_empty_account;
            $data['username']=$_SESSION['desired_username'];
            $data['email']=$_SESSION['email'];
            file_put_contents($userfile,serialize($data));
            $_SESSION['vestibule_message']=_('Account created').'<br>';
            }
        // changing password
        $data=unserialize(file_get_contents($userfile));
        $data['password']=crypt($_POST['password1'],'vestibule');
        file_put_contents($userfile,serialize($data));
        $_SESSION['vestibule_message'].=_('Password set for ').$data['username'];
        header("Location: $vestibule_homepage");
        exit;
    case 'vestibule_change_password':
                $data=unserialize(file_get_contents("$vestibule_accounts/$user"));
                $_SESSION['username']=$user;
                $_SESSION['vestibule_message']=vestibule_send_mail($data['email']); // confirmation by mail for changing password
                header("Location: $vestibule_website");
    case 'vestibule_inscription':echo $vestibule_email_form;exit;
    case 'vestibule_mail':
            echo 'le mail est '.$_POST['mail'];
            if (!vestibule_filter_mail($_POST['mail'])){
                $_SESSION['vestibule_message']=_('Unauthorized email');
                $_SESSION['next_action']='vestibule_inscription';
                header("Location: $vestibule_website");
                }
            else{
                $_SESSION['vestibule_message']=vestibule_send_mail($_POST['mail']);
                header("Location: $vestibule_website");
                }
        exit;
    case 'vestibule_my_account':
        $userfile="$vestibule_accounts/$user";
        $data=unserialize(file_get_contents($userfile));
        echo '<h1>'._('Account').' '.$_SESSION['username'].'</h1>';
        echo $data['email'].'<br>';
        echo $vestibule_change_password;
        if ($user=='admin')
            echo '<pre>'.file_get_contents("$vestibule_accounts/admin/admin_messages").'</pre>';
        exit;
    }

if (isset($_COOKIE['duration']) and $_COOKIE['duration']!='dummy')
    vestibule_logout(_('timeout'));

