<style>
.nodec {text-decoration:none};
</style>
<div style="display:inline;position:relative;float:right"><a class='nodec' href="?lang=fr_FR.UTF-8">&#x1F1EB;&#x1F1F7;</a> <a class='nodec' href="?lang=en_EN.UTF-8">&#x1F1EC;&#x1F1E7;</a>&nbsp;</div>

<?php

function vestibule_grabvar($var,$default,&$v){
    global $_SESSION,$_GET;
    if (!isset($_SESSION[$var]))
        $_SESSION[$var]=$default;
    if (isset($_GET[$var])){
        $v=$_GET[$var];
        $_SESSION[$var]=$v;
        }
    else
        $v=$_SESSION[$var];
    }

vestibule_grabvar('lang','en_EN.UTF-8',$locale); 

putenv("LANG=".$locale); 
putenv('LC_ALL='.$locale);
setlocale(LC_ALL, $locale);
$domain='vestibule';
bindtextdomain($domain, $vestibule_path.'/locale');
bind_textdomain_codeset($domain, 'UTF-8');
textdomain($domain);

?>
