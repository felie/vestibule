# vestibule

Vestibule is a little system of connection for a website. Simple but can be used to a SSO system.

It furnishes creation of accounts with mail verification and change of password.

## Accounts

The accounts are stored in the *accounts* repository (protected by .htaccess if not yet). 
The *account* can override by an application. Usually a *users* repository will contain the user's data.

Tokens during registration are stored in *tokens* directory (protected by .htaccess also if not yet).

You should verify the .htaccess is read by Apache (see AllowOverride directive for that).

## Logic

If the server can send mail, the authentication by mail is used. Otherwise, a message is stored for the admin (he will see message in his account). The admin have to send the mails by himself.

![logic](auth/logic.svg)

## Installation

Rename *config_sample.php* into *config.php*

Out of the box! Plug and pray...

A admin account for the system is created if it does not exists.

## Usage

See the index for usage

$_SESSION variables contains the 'Login' of your user if he is connected.

## Goodies

System of translation. Be inspired, it's so simple. Poedit is your friend.

## License

GPLv3
